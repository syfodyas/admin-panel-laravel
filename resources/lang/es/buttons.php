<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate'           => 'Activar',
                'change_password'    => 'Cambiar Password',
                'clear_session'      => 'Borrar Session',
                'deactivate'         => 'Deactivar',
                'delete_permanently' => 'Eliminar Permanentemente',
                'login_as'           => 'Login como :user',
                'resend_email'       => 'Reenviar E-mail Confirmación',
                'restore_user'       => 'Restaurar User',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Confirmar Cuenta',
            'reset_password'  => 'Resetear Password',
        ],
    ],

    'general' => [
        'cancel'   => 'Cancelar',
        'continue' => 'Continuar',
        'preview'  => 'Previzualizar',

        'crud' => [
            'create' => 'Crear',
            'add'    => 'Agregar',
            'delete' => 'Eliminar',
            'edit'   => 'Editar',
            'update' => 'Actualizar',
            'view'   => 'Ver',
        ],

        'save' => 'Guardar',
        'view' => 'Ver',
    ],
];
