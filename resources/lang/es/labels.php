<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'Todo',
        'yes'     => 'si',
        'no'      => 'No',
        'custom'  => 'Personalizado',
        'actions' => 'Acciones',
        'active'  => 'Activo',
        'buttons' => [
            'save'   => 'Guardar',
            'update' => 'Actualizar',
        ],
        'hide'              => 'Ocultar',
        'inactive'          => 'Inactivo',
        'none'              => 'Nada',
        'show'              => 'Mostar',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'profile_updated' => 'Su perfil ha sido actualizado.',
        'access'          => [
            'roles' => [
                'create'     => 'Crear Role',
                'edit'       => 'Editar Role',
                'management' => 'Gestión de Roles',

                'table' => [
                    'number_of_users' => 'Número de Usuarios',
                    'permissions'     => 'Permisos',
                    'role'            => 'Role',
                    'sort'            => 'Ordenar',
                    'total'           => 'role total|roles total',
                ],
            ],

            'permissions' => [
                'create'     => 'Crear Permiso',
                'edit'       => 'Editar Permiso',
                'management' => 'Gestión de Permisos',

                'table' => [
                    'permission'   => 'Permiso',
                    'display_name' => 'Nombre Mostrado',
                    'sort'         => 'Ordenar',
                    'status'       => 'Status',
                    'total'        => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Usuarios Activos',
                'all_permissions'     => 'Todos los Permisos',
                'change_password'     => 'Cambiar Password',
                'change_password_for' => 'Cambiar Password para :user',
                'create'              => 'Crear User',
                'deactivated'         => 'Usuarios desactivados',
                'deleted'             => 'Usuarios eliminados',
                'edit'                => 'Editar User',
                'edit-profile'        => 'Editar Perfil',
                'management'          => 'Gestión de usuarios',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'      => 'Confirmado',
                    'created'        => 'Creado',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Última actualización',
                    'first_name'     => 'Nombre',
                    'last_name'      => 'Apellido',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'roles'          => 'Roles',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Vizualizar',
                        'history'  => 'Historial',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmado',
                            'created_at'   => 'Creado en',
                            'deleted_at'   => 'Eliminado en',
                            'email'        => 'E-mail',
                            'last_updated' => 'Última actualización',
                            'name'         => 'Nombre',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],

        'pages' => [
            'create'     => 'Create Page',
            'edit'       => 'Edit Page',
            'management' => 'Page Management',
            'title'      => 'Pages',

            'table' => [
                'title'     => 'Title',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'updatedat' => 'Updated At',
                'createdby' => 'Created By',
                'all'       => 'All',
            ],
        ],

        'blogcategories' => [
            'create'     => 'Create Blog Category',
            'edit'       => 'Edit Blog Category',
            'management' => 'Blog Category Management',
            'title'      => 'Blog Category',

            'table' => [
                'title'     => 'Blog Category',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'createdby' => 'Created By',
                'all'       => 'All',
            ],
        ],

        'blogtags' => [
            'create'     => 'Create Blog Tag',
            'edit'       => 'Edit Blog Tag',
            'management' => 'Blog Tag Management',
            'title'      => 'Blog Tags',

            'table' => [
                'title'     => 'Blog Tag',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'createdby' => 'Created By',
                'all'       => 'All',
            ],
        ],

        'blogs' => [
            'create'     => 'Create Blog',
            'edit'       => 'Edit Blog',
            'management' => 'Blog Management',
            'title'      => 'Blogs',

            'table' => [
                'title'     => 'Blog',
                'publish'   => 'PublishDateTime',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'createdby' => 'Created By',
                'all'       => 'All',
            ],
        ],

        'emailtemplates' => [
            'create'     => 'Create Email Template',
            'edit'       => 'Edit Email Template',
            'management' => 'Email Template Management',
            'title'      => 'Email Templates',

            'table' => [
                'title'     => 'Title',
                'subject'   => 'Subject',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'updatedat' => 'Updated At',
                'all'       => 'All',
            ],
        ],

        'settings' => [
            'edit'           => 'Edit Settings',
            'management'     => 'Settings Management',
            'title'          => 'Settings',
            'seo'            => 'SEO Settings',
            'companydetails' => 'Company Contact Details',
            'mail'           => 'Mail Settings',
            'footer'         => 'Footer Settings',
            'terms'          => 'Terms and Condition Settings',
            'google'         => 'Google Analytics Track Code',
        ],

        'faqs' => [
            'create'     => 'Create FAQ',
            'edit'       => 'Edit FAQ',
            'management' => 'FAQ Management',
            'title'      => 'FAQ',

            'table' => [
                'title'     => 'FAQs',
                'publish'   => 'PublishDateTime',
                'status'    => 'Status',
                'createdat' => 'Created At',
                'createdby' => 'Created By',
                'answer'    => 'Answer',
                'question'  => 'Question',
                'updatedat' => 'Updated At',
                'all'       => 'All',
            ],
        ],

        'menus' => [
            'create'     => 'Crear Menú',
            'edit'       => 'Editar Menú',
            'management' => 'Gestión de Menú',
            'title'      => 'Menus',

            'table' => [
                'name'      => 'Nombre',
                'type'      => 'Tipo',
                'createdat' => 'Creado en',
                'createdby' => 'Creado por',
                'all'       => 'Todo',
            ],
            'field' => [
                'name'      => 'Nombre',
                'type'      => 'Tipo',
                'items'     => 'Menú Items',
                'url'       => 'URL',
                'url_type'  => 'URL Tipo',
                'url_types' => [
                  'route'  => 'Route',
                  'static' => 'Static',
                ],
                'open_in_new_tab'    => 'Open URL in new tab',
                'view_permission_id' => 'Permission',
                'icon'               => 'Icon Class',
                'icon_title'         => 'Font Awesome Class. eg. fa-edit',
            ],
        ],

        'modules' => [
            'create'     => 'Crear Módulo',
            'management' => 'Gestión de Módulo',
            'title'      => 'Módulo',
            'edit'       => 'Editar Módulo',

            'table' => [
                'name'               => 'Nombre Módulo',
                'url'                => 'Module View Route',
                'view_permission_id' => 'Permisos',
                'created_by'         => 'Creado por',
            ],

            'form' => [
                'name'                  => 'Module Name',
                'url'                   => 'View Route',
                'view_permission_id'    => 'View Permission',
                'directory_name'        => 'Directory Name',
                'namespace'             => 'Namespace',
                'model_name'            => 'Model Name',
                'controller_name'       => 'Controller &nbsp;Name',
                'resource_controller'   => 'Resourceful Controller',
                'table_controller_name' => 'Controller &nbsp;Name',
                'table_name'            => 'Table Name',
                'route_name'            => 'Route Name',
                'route_controller_name' => 'Controller &nbsp;Name',
                'resource_route'        => 'Resourceful Routes',
                'views_directory'       => 'Directory &nbsp;&nbsp;&nbsp;Name',
                'index_file'            => 'Index',
                'create_file'           => 'Create',
                'edit_file'             => 'Edit',
                'form_file'             => 'Form',
                'repo_name'             => 'Repository Name',
                'event'                 => 'Event Name',
            ],
        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login con :social_media',
            'register_box_title' => 'Registro',
            'register_button'    => 'Registro',
            'remember_me'        => 'Recordarme',
        ],

        'passwords' => [
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Country Alpha Codes',
                'alpha2'  => 'Country Alpha 2 Codes',
                'alpha3'  => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Cambiar Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Creado en',
                'edit_information'   => 'Editar Información',
                'email'              => 'E-mail',
                'last_updated'       => 'Última actualización',
                'first_name'         => 'Nombre',
                'last_name'          => 'Apellido',
                'address'            => 'Dirección',
                'state'              => 'Provincia',
                'city'               => 'Ciudad',
                'zipcode'            => 'Código Postal',
                'ssn'                => 'SSN',
                'update_information' => 'Actualizar Información',
            ],
        ],

    ],
];
